const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CourseController = require('../controllers/course')

//Adding a Course
router.post('/', auth.verify, (req, res) => {
    CourseController.add(req.body).then(result => res.send(result))
})

//Retrieve all course
router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses))
})


//Retrieve individual courses
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
    CourseController.get({ courseId }).then(course => res.send(course)) 
})

//Updating a course
router.put('/:courseId', auth.verify, (req, res) => {
    CourseController.update(req.body).then(result => res.send(result))
})

//Soft Deleting a course
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
    CourseController.archive({ courseId }).then(result => res.send(result))
})

//Changing the Course if Active or Not
router.put('/:courseId', auth.verify, (req, res) => {
	CourseController.change(req.body).then(changes => res.send(changes))
})

module.exports = router