const Course = require('../models/Course')


//Adding a course

module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}


//Retrieve all course
module.exports.getAll = () => {
	return Course.find({ isActive: true }).then(courses => courses)
}


//Retrieve individual courses
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

//Updating a Course
module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

//Soft Deleting a course
module.exports.archive = (params) => {
	const updates = { isActive: false }

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}


//Changing the course if Active or not
module.exports.change = (params) => {
	const changes = {
		isActive : params.isActive
	}
}