/*console.log(window.location.search);*/

// Instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);

/*console.log(params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));*/

let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let enrolleesContainer = document.querySelector("#enrolleesContainer");


//Retrieve the user information for isAdmin 
let adminUser = localStorage.getItem("isAdmin");
console.log(adminUser);

let token = localStorage.getItem('token');
/*console.log(userToken);*/

/*if (adminUser === false || !adminUser){*/

fetch(`http://localhost:4000/api/course/${courseId}`)
.then(res => res.json())
.then(data => {

	if (adminUser === "false" || !adminUser) {

	console.log(data);
	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = 
	`
		<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
	`

	document.querySelector("#enrollButton").addEventListener("click", () => {
		fetch('http://localhost:4000/api/users/enroll',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			},

			body: JSON.stringify({
				courseId: courseId
			})	
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true) {
				alert("Thank you for enrolling! See you in class!");
				window.location.replace("courses.html");
			} else {
				alert("Something went wrong");
			}
		})
	})

	} else {
		let enrolleesHeader = 
		`
		<h2>
		Students Enrolled
		</h2>
		<h2>Name</h2>

		`

		let enrolleeList = data.enrollees.map(enrollees => {
			console.log(enrollees)
			
			return(`
						<li>${enrollees.userId}</li>
					`)
		})
		
		allEnrollees = enrolleeList.join("");


 		enrolleesContainer.innerHTML = enrolleesHeader + allEnrollees;
	}
})