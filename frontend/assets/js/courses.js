
//Retrieve the user information for isAdmin 
let adminUser = localStorage.getItem("isAdmin");

console.log(adminUser);
//Will contain the html for the different buttons
let cardFooter;

let enabler = document.querySelector("#enabler");

fetch('http://localhost:4000/api/course')
.then(res => res.json())
.then(data => {
	console.log(data);

	// Variable to store the card/message to show if there are no courses
	let courseData;

	if (data.length < 1) {
		courseData = "No Courses Available"
	} else {
		courseData = data.map(course => {

			//Logic for rendering different buttons based on the user
			if (adminUser === "false" || !adminUser){
				//Relative Path?parameter_name = value
				// ./course.html?courseId=${course._id}
				cardFooter = 
				`
					<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
				`
			} else {
				cardFooter = 
				`
				<button id="enabler" class="btn btn-primary text-white btn-block editButton"> Enable / Disable Course</button>

				<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Students Enrolled </a>

				<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
	                            
                <a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>
				`
			}


			return(

			`
			<div class="col-md-6 my-3">
                <div class='card'>
                    <div class='card-body'>
                        <h5 class='card-title'>${course.name}</h5>
                        <p class='card-text text-left'>
                            ${course.description}
                        </p>
                        <p class='card-text text-right'>
                           ₱ ${course.price}
                        </p>

                    </div>
                    <div class='card-footer'>
                        ${cardFooter}
                    </div>
                </div>
            </div>
			`
			)
		}).join("")

		let coursesContainer = document.querySelector("#coursesContainer");

		coursesContainer.innerHTML = courseData;
	}
})

let modalButton = document.querySelector("#adminButton");

if (adminUser === "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
			</div>
		`
}


function courseEnable () {
    let course = 
    fetch(`http://localhost:4000/api/course/showcourses`)
    .then(res => res.json())
    .then(data => {
        console.log(data);
        enabler.addEventListener('click', function (e){
        	console.log('Hello World');
        })
    })

}

courseEnable();