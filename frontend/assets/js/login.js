let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;
	console.log(email);
	console.log(password);

	if(email == "" || password =="") {
		alert("Please input your email and/or password");
	} else {
		
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data.accessToken){
				//Store the token in local storage
				localStorage.setItem('token', data.accessToken);
				//Local Storage {
				// token: data.accessToken
				// }

				//Fetch request to get the details of the user
				fetch("http://localhost:4000/api/users/details", {
					headers: {
						'Authorization' : `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					//Store the user details in the user storage for future use
					localStorage.setItem('id', data._id);
					localStorage.setItem('isAdmin', data.isAdmin);

					//Redirect the user to our courses page
					window.location.replace("courses.html");

				})
			}	else {

				alert("Something went wrong");
			}
		})
	}
})