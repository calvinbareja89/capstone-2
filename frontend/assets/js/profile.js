let token = localStorage.getItem("token");
let profileContainer = document.querySelector("#profileDetails");
let profileCourses = document.querySelector("#courseDetails");
let adminDiv = document.querySelector("#adminDiv");

//Retrieve the user information for isAdmin 
let adminUser = localStorage.getItem("isAdmin");
/*console.log(adminUser);*/


//Function that calls the function
showuser();
showcourses();


//Function that shows the user
function showuser() {
	fetch('http://localhost:4000/api/users/details', {
		method: 'GET',
		headers: {
			'Authorization' : `Bearer ${token}`
		}
	}) 
	.then(res => res.json())
	.then(data => {
		
		//Container for user profile
		let profile;

		if (data.length === 0) {
			profile = alert("No details to display");
		} else {

			profile = 
			`
			<div id="profileDiv">
			<h2>Name: ${data.firstName} ${data.lastName}</h2>
			<h3>Mobile Number: ${data.mobileNo}</h3>
			<h3>Email: ${data.email}</h3>
			</div>

			`;
				
		}

		profileContainer.innerHTML = profile;
		
	});
}

//Function that shows the Users enrolled courses
function showcourses() {

	courseHeader = 
			`
			<div id="courseDiv">
			<h3>Course Name</h3><h3>Date Enrolled</h3><h3>Status</h3>
			</div>
			`;

	

	fetch('http://localhost:4000/api/users/details', {
		method: 'GET',
		headers: {
			'Authorization' : `Bearer ${token}`
		}
	}) 
	.then(res => res.json())
	.then(data => {

		let courseIds = data.enrollments.map(course => course.courseId)

		let promises = [];
		let courseNames;
		courseIds.forEach(courseId => {

			promises.push(
				fetch(`http://localhost:4000/api/course/${courseId}`)
				.then(res => res.json())
				.then(data => {
					return data.name;
				})
			)


			let courseEnrolledon = data.enrollments.map(courseEnrolled => {
				return(`
					<div id="courseDiv2">
						<h4>${courseEnrolled.enrolledOn}</h4>
						<h4>${courseEnrolled.status}</h4>
					</div>`);

			});

			Promise.all(promises)
			.then(function handleData(data){
				console.log(data)
				courseNames = data.map(course => {
					console.log(course)
						return (`<h4>${course}</h4>`)
				})

				let allCourses = courseNames.join("");
				let allCourseEnrolledon = courseEnrolledon.join("");
				profileCourses.innerHTML = courseHeader + allCourses + allCourseEnrolledon;
			})
		})
		
	})
		
}


	